- Add `siac skynet isblocked` command as a helper to check if a skylink is
blocked since `siac skynet blocklist` returns a list of hashed merkleroots, so
the list cannot be visually verified.
